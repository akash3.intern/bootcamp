package com.example.productsbootcamp.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.productsbootcamp.ItemApplication
import com.example.productsbootcamp.ItemGridAdapter
import com.example.productsbootcamp.ItemListAdapter
import com.example.productsbootcamp.R
import com.example.productsbootcamp.databinding.ActivityMainBinding
import com.example.productsbootcamp.databinding.Fragment1Binding
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.models.ItemList
import com.example.productsbootcamp.viewModels.MainViewModel
import com.example.productsbootcamp.viewModels.MainViewModelFactory

class Fragment1() : Fragment() {

    private lateinit var binding: Fragment1Binding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ItemListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        binding = Fragment1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        adapter = ItemListAdapter()
        binding.recyclerview.adapter = adapter
        viewModel.items.observe(viewLifecycleOwner) {
            adapter.updateItem(it)
        }

    }

    fun filter(list: List<Item>) {
        adapter.updateItem(list)
    }
}
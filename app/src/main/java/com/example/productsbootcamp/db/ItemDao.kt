package com.example.productsbootcamp.db

import androidx.room.*
import com.example.productsbootcamp.models.Item

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addItem(item: Item)

    @Update
    suspend fun updateItem(item: Item)

    @Delete
    suspend fun deleteItem(item: Item)

    @Query("select * from item_table")
    fun getItems(): List<Item>

    @Query("select * from item_table where name = :name")
    fun getItemByName(name: String): Item?
}
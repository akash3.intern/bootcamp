package com.example.productsbootcamp

import android.app.Application
import com.example.productsbootcamp.api.ItemService
import com.example.productsbootcamp.api.RetrofitHelper
import com.example.productsbootcamp.db.ItemDatabase
import com.example.productsbootcamp.repository.ItemRepository

class ItemApplication: Application() {

    lateinit var itemRepository: ItemRepository

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

//    create the repository instance at a common place for access to different view models

    private fun initialize() {
        val itemService = RetrofitHelper.getInstance().create(ItemService::class.java)
        val database = ItemDatabase.getDatabase(applicationContext)
        itemRepository = ItemRepository(itemService, database, applicationContext)
    }
}
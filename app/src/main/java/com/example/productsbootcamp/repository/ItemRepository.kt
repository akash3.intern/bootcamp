package com.example.productsbootcamp.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.productsbootcamp.api.ItemService
import com.example.productsbootcamp.db.ItemDatabase
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.utils.NetworkUtils

class ItemRepository(
    private val itemService: ItemService,
    private val itemDatabase: ItemDatabase,
    private val applicationContext: Context
) {

    private val itemsLiveData = MutableLiveData<List<Item>>()
    private val itemDao = itemDatabase.itemDao()

    val items: LiveData<List<Item>>
        get() = itemsLiveData

    fun getItems(){
        itemsLiveData.postValue(itemDao.getItems())
    }

    suspend fun updateDatabase(items: List<Item>){
        items.forEach { item ->
            if(itemDao.getItemByName(item.name) == null){
                itemDao.addItem(item)
            }
        }
    }

    suspend fun syncDatabase(){
        if(NetworkUtils.isInternetAvailable(applicationContext)){
            val result = itemService.getItems()
            if(result?.body() != null){
                updateDatabase(result.body()!!.data.items)
            }
        }
    }

}
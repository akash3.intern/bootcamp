package com.example.productsbootcamp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.productsbootcamp.databinding.ListItemBinding
import com.example.productsbootcamp.models.Item

class ItemListAdapter() : RecyclerView.Adapter<ItemViewHolder>() {

    private val items= ArrayList<Item>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = items[position]
        holder.titleView.text = currentItem.name
        holder.priceView.text = currentItem.price
        holder.shippingView.text = if(currentItem.extra == "null") " " else currentItem.extra
        holder.image.setImageResource(R.drawable.color_rectangle)
    }

    fun updateItem(updateItems: List<Item>){
        items.clear()
        items.addAll(updateItems)
        notifyDataSetChanged()
    }

}

class ItemViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root){
    val titleView = binding.title
    val priceView = binding.price
    val shippingView = binding.shipping
    val image = binding.imageView
}

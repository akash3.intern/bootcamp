package com.example.productsbootcamp.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.models.ItemList
import com.example.productsbootcamp.repository.ItemRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.Serializable

class MainViewModel(private val repository: ItemRepository): ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.syncDatabase()
            repository.getItems()
        }
    }

    val items : LiveData<List<Item>>
    get() = repository.items

}